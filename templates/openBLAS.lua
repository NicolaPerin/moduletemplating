-- SPDX-FileCopyrightText: (C) 2024 Niccolò Tosato <niccolo.tosato@areasciencepark.it>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later


-- -*- lua -*-

local name      = "openBLAS"
local version   = "{{version}}"

whatis("Name         : " .. name)
whatis("Version      : " .. version)

family("BLAS")

local home    = "/opt/programs"

home=home .. "/" .. name .. "/" .. version


prepend_path{"PATH", home .. "/bin",delim=":",priority="0"}
prepend_path{"LD_LIBRARY_PATH", home .. "/lib",delim=":",priority="0"}
prepend_path{"LIBRARY_PATH", home .. "/lib",delim=":",priority="0"}
prepend_path{"CPATH", home .. "/include",delim=":",priority="0"}
setenv("OPENBLAS_DIR", home )
setenv("OPENBLAS_ROOT", home)
setenv("OPENBLAS_LIB", home .. "/lib")
setenv("OPENBLAS_IN",home .. "/include")
setenv("OPENBLAS_INCLUDE",home .. "/include")
