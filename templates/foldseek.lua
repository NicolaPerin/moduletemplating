-- SPDX-FileCopyrightText: (C) 2024 Niccolò Tosato <niccolo.tosato@areasciencepark.it>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later


-- -*- lua -*-

local name      = "foldseek"
local version   = "{{version}}"
whatis("Name         : " .. name)
whatis("Version      : " .. version)

family("foldseek")

--conflict()

--In this section all dependencies will be specified

--binary location without architecture extension
local home    = "/opt/programs"

home=home .. "/" .. name .. "/" .. version

prepend_path("PATH", home .. "/bin")


