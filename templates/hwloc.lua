-- SPDX-FileCopyrightText: (C) 2024 Niccolò Tosato <niccolo.tosato@areasciencepark.it>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later


-- -*- lua -*-

local name      = "hwloc"
local version   = "{{version}}"

whatis("Name         : " .. name)
whatis("Version      : " .. version)

family("hwloc")

local home    = "/opt/programs"

home=home .. "/" .. name .. "/" .. version


--prepend-path	MANPATH			$home/share/man
--prepend-path	PKG_CONFIG_PATH		$home/lib/pkgconfig



prepend_path{"PATH", home .. "/bin",delim=":",priority="0"}
prepend_path{"LD_LIBRARY_PATH", home .. "/lib",delim=":",priority="0"}
prepend_path{"MANPATH",home .. "/share/man"}
prepend_path{"PKG_CONFIG_PATH",home .. "/lib/pkgconfig"}

setenv("HWLOC_HOME", home )
