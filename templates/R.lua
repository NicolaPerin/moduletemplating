-- SPDX-FileCopyrightText: (C) 2024 Niccolò Tosato <niccolo.tosato@areasciencepark.it>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later


-- -*- lua -*-

local name      = "R"
local version   = "{{version}}"
whatis("Name         : " .. name)
whatis("Version      : " .. version)

family("R")
depends_on("openBLAS")

--conflict()

--In this section all dependencies will be specified

--binary location without architecture extension
local home    = "/opt/programs"

home=home .. "/" .. name .. "/" .. version

prepend_path("PATH", home .. "/bin")
prepend_path("LD_LIBRARY_PATH", home .."/lib64")
prepend_path("MANPATH", home .."/share/man")


