-- SPDX-FileCopyrightText: (C) 2024 Niccolò Tosato <niccolo.tosato@areasciencepark.it>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later


-- -*- lua -*-

local name      = "fastqc"
local version   = "{{version}}"

whatis("Name         : " .. name)
whatis("Version      : " .. version)

family("fastqc")

--In this section all dependencies will be specified
depends_on("java/1.8.0")

--binary location without architecture extension
local home    = "/opt/programs"

home=home .. "/" .. name .. "/" .. version

prepend_path("PATH", home)
