-- SPDX-FileCopyrightText: (C) 2024 Niccolò Tosato <niccolo.tosato@areasciencepark.it>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later


-- -*- lua -*-

local name 	= "picard"
local version 	= "{{version}}"

family("picard")
depends_on("java/17.0.10")
depends_on("R")


whatis("Name         : " .. name)
whatis("Version      : " .. version)


local home    = "/opt/programs"

home=home .. "/" .. name .. "/" .. version



setenv("picard", home .. "/picard.jar")
set_alias("PICARD", "java -jar " .. home .. "/picard.jar")


if (mode() == "load") then
    LmodMessage(name .. " " .. version .. "\nUSAGE1:\t java -jar $picard")
    LmodMessage("USAGE2: PICARD")
end


