-- SPDX-FileCopyrightText: (C) 2024 Niccolò Tosato <niccolo.tosato@areasciencepark.it>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later


-- -*- lua -*-

local name      = "bedtools2"
local version   = "{{version}}"

whatis("Name         : " .. name)
whatis("Version      : " .. version)

family("bedtools2")

local home    = "/opt/programs"

home=home .. "/" .. name .. "/" .. version

prepend_path{"PATH", home .. "/bin"}
