-- SPDX-FileCopyrightText: (C) 2024 Niccolò Tosato <niccolo.tosato@areasciencepark.it>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later


-- -*- lua -*-

local name      = "fastp"
local version   = "{{version}}"
whatis("Name         : " .. name)
whatis("Version      : " .. version)

family("fastp")

--conflict()

--In this section all dependencies will be specified

--binary location without architecture extension
local home    = "/opt/programs"

prepend_path("PATH", home .. "/libisal/2.30/bin")
prepend_path("LD_LIBRARY_PATH", home .. "/libisal/2.30/lib")


home=home .. "/" .. name .. "/" .. version

prepend_path("PATH", home .. "/bin")

