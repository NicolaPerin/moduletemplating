-- SPDX-FileCopyrightText: (C) 2024 Niccolò Tosato <niccolo.tosato@areasciencepark.it>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later


-- -*- lua -*-

local name 	= "bcl2fastq2"
local version 	= "{{version}}"

whatis("Name         : " .. name)
whatis("Version      : " .. version)
family("bcl2fastq2")


local home    = "/opt/programs"

home=home .. "/"  .. name .. "/" .. version
prepend_path("BCL2FASTQ_HOME",home)
prepend_path("PATH", home .. "/bin")
prepend_path("LD_LIBRARY_PATH", home .. "/lib")


