# SPDX-FileCopyrightText: (C) 2024 Niccolò Tosato <niccolo.tosato@areasciencepark.it>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import argparse
from jinja2 import Environment, FileSystemLoader, select_autoescape

def compile_template(template_file, context):
    env = Environment(
        loader=FileSystemLoader('templates'),
    )
    template = env.get_template(template_file)
    rendered_template = template.render(context)
    return rendered_template

def write_module(content,path):
    with open(f"{path}.lua", mode="w", encoding="utf-8") as module:
        module.write(content)
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Lua module file generator.')
    parser.add_argument('--version', type=str, help='Module version')
    parser.add_argument('--module', type=str, help='Module name')
    parser.add_argument('--path', type=str, help='Module file path')
    args = parser.parse_args()
    version = args.version
    path = args.path
    name = args.module
    template_file = f'{name}.lua'
    context = {
        'version': version,
    }
    compiled_template = compile_template(template_file, context)
    write_module(compiled_template,f"{path}/{version}")
